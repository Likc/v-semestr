﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace GUI_TC
{
    public partial class Form3 : Form
    {
        private readonly Form1 form;
        String MyConString = "SERVER=localhost;" +
                            "DATABASE=tc;" +
                            "UID=root;" +
                            "PASSWORD=;" +
                            "charset=utf8";
        public Form3(Form1 form)
        {
            this.form = form;
            InitializeComponent();
            String SQL = "SELECT * FROM `streets` order by street_name";
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            comboBox1.Items.Add(reader.GetString("street_name"));
                        }
                    }
                }
            }
        }

        private void Search_Click(object sender, EventArgs e)
        {
      
            string MaxCN;
            String street_id,client_id,phone_id;

            String SQL = "SELECT max(contract_num) from clients";
            //номер контракта
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    MaxCN = Convert.ToString(cmd.ExecuteScalar());
                }
            }

            //id улицы
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    cmd.CommandText = String.Format("Select id from streets where street_name = '{0}'",comboBox1.SelectedItem.ToString());
                    street_id = Convert.ToString(cmd.ExecuteScalar());
                }
            }
            //проверка на наличие адреса
            SQL =String.Format("Select id from v_clients where (street_id = '{0}') and (house_number = '{1}') and (block = '{2}') and (flat = '{3}')", street_id, textBox4.Text,textBox5.Text,textBox6.Text);
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                { 
                    String is_in_base = Convert.ToString(cmd.ExecuteScalar());
                    if (is_in_base != "")
                    {
                        MessageBox.Show("Данный адрес уже в базе");
                        form.Visible = true;
                        this.Close();
                        return;
                    }
                }
            }

            //добавление клиента
            SQL = String.Format("INSERT INTO `clients` (`id`, `first_name`, `second_name`, `surname`, `birthday`, `street_id`, `house_number`, `block`, `flat`, `contract_num`, `activ`)" +
                " VALUES (NULL, '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '1')",
                textBox1.Text,
                textBox2.Text,
                textBox3.Text,
                maskedTextBox2.Text,
                street_id,
                textBox4.Text,
                textBox5.Text,
                textBox6.Text,
                Convert.ToInt32(MaxCN)+1);
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }

            //вставка номера
            maskedTextBox1.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            SQL = String.Format("INSERT INTO `phones` (`id`, `phone_number`) VALUES (NULL, '{0}')",maskedTextBox1.Text);
            maskedTextBox1.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch { }
                }
            }

            //получение id номера
            maskedTextBox1.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            SQL = String.Format("Select id from phones where phone_number = '{0}';", maskedTextBox1.Text);
            maskedTextBox1.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    phone_id = Convert.ToString(cmd.ExecuteScalar());
                }
            }

            //получение id клиента 
            SQL = "Select max(id) from clients;";
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    client_id = Convert.ToString(cmd.ExecuteScalar());
                }
            }

            //добавление связки клиент-телефон
            SQL = String.Format("INSERT INTO `phones_clients` (`client_id`, `phone_id`) VALUES ('{0}', '{1}');",client_id,phone_id);
            using (MySqlConnection conn = new MySqlConnection(MyConString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SQL, conn))
                {
                    cmd.ExecuteNonQuery();
                }
            }

            form.Visible = true;
            this.Close();
        }
    }
}
