﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace GUI_TC
{
    public partial class Form2 : Form
    {
        private readonly Form1 form;
        String id;
        int pos;
        DataGridViewSelectedCellCollection col;
        String MyConString = "SERVER=localhost;" +
            "DATABASE=tc;" +
            "UID=root;" +
            "PASSWORD=;" +
            "charset=utf8";
        MySqlCommand cmd = null;
        List<String> phones_ids;
        List<String> phones_to_del;
        List<String> phones_nubmers_to_del;
        public Form2(Form1 form, DataGridViewSelectedCellCollection col, String id)
        {
            this.form = form;
            this.col = col;
            this.id = id;
            InitializeComponent();

            phones_to_del = new List<string>();
            phones_nubmers_to_del = new List<string>();
            richTextBox1.Text = col[0].Value.ToString();
            pos = col[0].ColumnIndex;
            maskedTextBox1.Visible = false;
            comboBox1.Visible = false;
            listBox1.Visible = false;
            button1.Visible = false;
            button2.Visible = false;
            MySqlConnection conn = new MySqlConnection(MyConString);

            if (pos == 3)
            {
                richTextBox1.Visible = false;
                maskedTextBox1.Visible = true;
                maskedTextBox1.Mask = "0000-00-00";
                DateTime dt;
                dt = DateTime.Parse(col[0].Value.ToString());                
                maskedTextBox1.Text = dt.ToString("yyyy-MM-dd");

            }
            else if (pos == 4)
            {
                //street_name
                richTextBox1.Visible = false;
                comboBox1.Visible = true;
                String SQL = "SELECT * FROM `streets` order by street_name";
                cmd = new MySqlCommand(SQL, conn);
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        comboBox1.Items.Add(reader.GetString("street_name"));
                    }
                }
                conn.Close();
            }
            else if (pos == 8)
            {
                //phones_numbers
                richTextBox1.Visible = false;
                maskedTextBox1.Visible = true;
                listBox1.Visible = true;
                button1.Visible = true;
                button2.Visible = true;
                maskedTextBox1.Mask = "0-(000)-000-00-00";
                String[] mass =  col[0].Value.ToString().Split(',');
                phones_ids = new List<string>();
                conn.Open();
                foreach (var elem in mass)
                {
                    listBox1.Items.Add(elem);
                    String SQL = String.Format("SELECT id " +
                            "FROM phones " +
                            "WHERE phone_number={0} ", elem);
                    cmd = new MySqlCommand(SQL, conn);
                    phones_ids.Add(Convert.ToString(cmd.ExecuteScalar()));
                }
                conn.Close();
                listBox1.SelectedIndex = 0;
                //maskedTextBox1.Text = col[0].Value.ToString();
            }

        }

        private void Search_Click(object sender, EventArgs e)
        {
            MySqlConnection conn = new MySqlConnection(MyConString);
            if (pos == 1)
            {
                //contract_num
                cmd = new MySqlCommand("", conn);
                conn.Open();
                cmd.CommandText =String.Format("SELECT contract_num " +
                    "FROM v_clients " +
                    "WHERE contract_num={0} ",richTextBox1.Text);
                string str = Convert.ToString(cmd.ExecuteScalar());
                if (str == "")
                {
                    String SQL = String.Format("UPDATE `v_clients` SET " +
                        "`contract_num` = '{0}' " +
                        "WHERE `v_clients`.`id` = {1}", richTextBox1.Text, id);
                    cmd = new MySqlCommand(SQL, conn);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    MessageBox.Show("Данный номер договора уже в базе");
                }
                conn.Close();
            }
            else if (pos == 3)
            {
                //birthday
                conn.Open();
                DateTime dt;
                if (DateTime.TryParse(maskedTextBox1.Text, out dt))
                {
                    String SQL = String.Format("UPDATE `v_clients` SET " +
                            "`birthday` = '{0}' " +
                            "WHERE `v_clients`.`id` = {1}", maskedTextBox1.Text, id);
                    cmd = new MySqlCommand(SQL, conn);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    MessageBox.Show("Неверная дата");
                }
                conn.Close();
            }
            else if (pos == 4)
            {
                //street_name
                conn.Open();
                String SQL = String.Format("UPDATE `v_clients` SET " +
                    "`street_name` = '{0}' " +
                    "WHERE `v_clients`.`id` = {1}", comboBox1.SelectedItem.ToString(), id);
                cmd = new MySqlCommand(SQL, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else if (pos == 5)
            {
                //house_number
                conn.Open();
                String SQL = String.Format("UPDATE `v_clients` SET " +
                    "`house_number` = '{0}' " +
                    "WHERE `v_clients`.`id` = {1}", richTextBox1.Text, id);
                cmd = new MySqlCommand(SQL, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else if (pos == 6)
            {
                //block
                conn.Open();
                String SQL = String.Format("UPDATE `v_clients` SET " +
                    "`block` = '{0}' " +
                    "WHERE `v_clients`.`id` = {1}", richTextBox1.Text, id);
                cmd = new MySqlCommand(SQL, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else if (pos == 7)
            {
                //flat
                conn.Open();
                String SQL = String.Format("UPDATE `v_clients` SET " +
                    "`flat` = '{0}' " +
                    "WHERE `v_clients`.`id` = {1}", richTextBox1.Text, id);
                cmd = new MySqlCommand(SQL, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else if (pos == 8)
            {
                //phones_numbers
                conn.Open();
                for (int i = 0; i<listBox1.Items.Count; i++)
                {
                    String SQL = String.Format("UPDATE `phones` SET " +
                        "`phone_number` = '{0}' " +
                        "WHERE `id` = {1}", listBox1.Items[i], phones_ids[i]);
                    cmd = new MySqlCommand(SQL, conn);
                    cmd.ExecuteNonQuery();
                }
                if (phones_to_del.Count != 0)
                {
                    foreach (var elem in phones_to_del)
                    {
                        String SQL = String.Format("DELETE FROM `phones` WHERE id = {0}", elem);
                        cmd = new MySqlCommand(SQL, conn);
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
            else if (pos == 9)
            {
                //activ
            }

            form.Visible = true;
            form.Search_Click(null,null);
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                maskedTextBox1.Text = listBox1.SelectedItem.ToString();
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            maskedTextBox1.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            listBox1.Items[listBox1.SelectedIndex] = maskedTextBox1.Text;
            maskedTextBox1.TextMaskFormat = MaskFormat.IncludeLiterals;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("Вы точно хотите удалить данный номер?","Удаление",MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
                phones_to_del.Add(phones_ids[listBox1.SelectedIndex]);
                phones_ids.RemoveAt(listBox1.SelectedIndex);
                phones_nubmers_to_del.Add(listBox1.Items[listBox1.SelectedIndex].ToString());
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }


        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {
            if (listBox1.Visible == true)
            {
                if (e.Control == true && e.KeyCode == Keys.Z)
                {
                    try
                    {
                        listBox1.Items.Add(phones_nubmers_to_del.Last());
                        phones_ids.Add(phones_to_del.Last());
                        phones_nubmers_to_del.RemoveAt(phones_nubmers_to_del.Count - 1);
                        phones_to_del.RemoveAt(phones_to_del.Count - 1);
                    }
                    catch
                    { }
                }
            }
        }

    }
}
