﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.Common;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
namespace GUI_TC
{
    public partial class Form1 : Form
    {
        String MyConString = "SERVER=localhost;" +
                    "DATABASE=tc;" +
                    "UID=root;" +
                    "PASSWORD=;" +
                    "charset=utf8";
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Grid.Visible = false;
        }


        private void клиентToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = true;
        }


        public void Search_Click(object sender, EventArgs e)
        {
            MySqlConnection conn = new MySqlConnection(MyConString);
            MySqlCommand cmd = null;
            String SourName, PhoneNumber, ContractNum;
            SourName = SourNameText.Text;
            PhoneNumber = PhoneNumberText.Text;
            ContractNum = ContractNumText.Text;
            String SQL = String.Format("SELECT id,contract_num,fio,birthday,street_name,house_number,block,flat,phones_numbers,activ" +
                " FROM `v_clients` WHERE (fio LIKE '%{0}%') and" +
                "(phones_numbers LIKE  '%{1}%') and " +
                "(contract_num LIKE '%{2}%');", SourName, PhoneNumber, ContractNum);
            cmd = new MySqlCommand(SQL, conn);
            if (SourName == "" && PhoneNumber == "" && ContractNum == "")
                MessageBox.Show("Нет фильтра для отбора");
            else
            {
                conn.Open();
                DataTable dataTable = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dataTable);
                Grid.DataSource = dataTable;
                Grid.Visible = true;
                try
                {
                    Grid.ReadOnly = true;
                    Grid.Columns[0].Visible = false;
                }
                catch { };
                conn.Close();
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.tabControl1.SelectedIndex)
            {
                case 1:
                    this.AcceptButton = this.button1;
                    MySqlConnection conn = new MySqlConnection(MyConString);
                    MySqlCommand cmd = null;
                    String SQL = String.Format("SELECT creation_time,statement_deadline,ending_time,state_ident,type,solution,fio,street_name,house_number,block,flat,phones_numbers,contract_num,executor,creator FROM v_statements");
                    cmd = new MySqlCommand(SQL, conn);
                    conn.Open();
                    DataTable dataTable = new DataTable();
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dataTable);
                    dataGridView1.DataSource = dataTable;
                    conn.Close();
                    break;
                case 0:
                default:
                    this.AcceptButton = this.Search;
                    break;
            }
        }

        private void редактированиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void Grid_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var res = MessageBox.Show("Ходите редактировать выбраное поле", "Редактировать?", MessageBoxButtons.YesNo);

            if (res == DialogResult.Yes)
            {
                var col = Grid.SelectedCells;
                int pos = col[0].ColumnIndex;
                if (pos != 2)
                {
                    String id = Grid.Rows[col[0].RowIndex].Cells[0].Value.ToString();
                    this.Visible = false;
                    Form2 Form2 = new Form2(this, col, id);
                    Form2.Show();
                }
                else
                {
                    MessageBox.Show("Данный функционал пока недоступен");
                }
            }
        }

        private void Grid_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var res = MessageBox.Show("Точно хотите удалить данную запись?", "Удаление", MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
                var col = Grid.SelectedRows[0].Index;
                String id = Grid.Rows[col].Cells[0].Value.ToString();
                MySqlConnection conn = new MySqlConnection(MyConString);
                MySqlCommand cmd = null;
                String SQL = String.Format("DELETE FROM `clients` WHERE id = {0}", id);
                conn.Open();
                cmd = new MySqlCommand(SQL, conn);
                cmd.ExecuteNonQuery();
                SQL = String.Format("DELETE FROM `phones_clients` WHERE client_id = {0}", id);
                cmd = new MySqlCommand(SQL, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked || checkBox2.Checked || checkBox3.Checked)
            {
                String type1 ="", type2="", type3="";
                if (checkBox1.Checked)
                {
                    type1 = "Выполнено";
                }
                if (checkBox2.Checked)
                {
                    type2 = "Просрочено";
                }
                if (checkBox3.Checked)
                {
                    type3 = "В работе";
                }
                MySqlConnection conn = new MySqlConnection(MyConString);
                String SQL = String.Format("SELECT creation_time, statement_deadline, ending_time, state_ident, type, solution, fio, street_name, house_number, block, flat, phones_numbers, contract_num, executor, creator " +
                    "FROM v_statements WHERE(state_ident in ('{0}', '{1}', '{2}')); ",type1,type2,type3);
                MySqlCommand cmd = new MySqlCommand(SQL, conn);
                conn.Open();
                DataTable dataTable = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dataTable);
                dataGridView1.DataSource = dataTable;
                conn.Close();
            }
        }

        private void клиентаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form3 form3 = new Form3(this);
            form3.Show();
        }
    }
}
