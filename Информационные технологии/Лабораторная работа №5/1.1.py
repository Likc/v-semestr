import os
import fire

def my_ls(firstparam=None):
    print(firstparam)
    if (firstparam==None):
        print("Result:\n")
        os.system('where /R "%cd%" *')
    else:
        print('Result:\n')
        os.system('where /R "%cd%" {param}'.format(param=firstparam))

if __name__ == '__main__':
  fire.Fire(my_ls)