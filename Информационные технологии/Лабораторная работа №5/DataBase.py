from termcolor import colored

def printlist():
    with open('DataBase.txt','r') as f:
        cash = f.readlines()
        for i in range(len(cash)):
            print(i+1,cash[i])
    return menu()

def edition():
    sourname = input('Enter sourname:')
    name = input('Enter name:')
    year = input('Enter your year of birth:')
    city = input('Enten your place of birth:')
    sex = input('Enten your sex (M|F):')
    with open('DataBase.txt','a') as f:
        f.writelines([sourname,"\t",name,"\t",year,"\t",city,"\t",sex,"\n"])
    printlist()

def delete():
    delet = input('What to delete:')
    delet = delet.split(' ')
    with open('DataBase.txt','r') as f:
        cash = f.readlines()
    with open('DataBase.txt','w') as f:
        for i in range(len(cash)):
            if str(int(i)+1) in delet:
                continue
            else:
                f.write(cash[i])
    printlist()

def sorting():
    tmp = []
    res = []
    with open('DataBase.txt','r') as f:
        cash = f.readlines()
    for i in range(len(cash)):
        tmp.append(cash[i].split())
    ans = input('Chouse param. for sort:\n 1)Sourname 2)Name 3)Year of birth 4)City 5)Sex\n')
    if ans == '1':
        tmp = sorted(tmp,key=lambda x: x[0])
    elif ans == '2':
        tmp = sorted(tmp, key=lambda x: x[1])
    elif ans == '3':
        tmp = sorted(tmp, key=lambda x: x[2])
    elif ans == '4':
        tmp = sorted(tmp, key=lambda x: x[3])
    elif ans == '5':
        tmp = sorted(tmp, key=lambda x: x[4])
    else:
        print('Wrong answer')
    for i in range(len(tmp)):
        res.append('\t'.join(tmp[i]))
    with open('DataBase.txt','w') as f:
        f.write('\n'.join(res))
    printlist()


def menu():
    print(colored('1)','red'),'list of students',colored('2)','blue'),'Edit students',colored('3)','yellow'),'Delete',colored('4)','green'),'Sort by',colored('5)','magenta'),'Exit')
    ans = input(colored('What to do:\n','cyan'))
    if ans == '1':
        printlist()
    elif ans == '2':
        edition()
    elif ans == '3':
        delete()
    elif ans == '4':
        sorting()
    elif ans == '5':
        exit()
    else:
        print('Wrong ans:')
        menu()
menu()