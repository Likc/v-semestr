import os
import fire

def my_ls(firstparam=None,secondparam=None):
    print('Result:\n')
    os.system('type {fparam}\*.{sparam} | more'.format(fparam=firstparam,sparam=secondparam))

if __name__ == '__main__':
  fire.Fire(my_ls)