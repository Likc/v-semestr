import wmi
import psutil
import re
c = wmi.WMI ()
k=0
print('All processes:')
for process in c.Win32_Process():
    k+=1
    print(k, 'Name = '+process.Name, 'Id = '+str(process.ProcessId), 'Priority = '+str(process.Priority), sep = '\t')
while True:
    res = input('What process dlls you want to see?\n')
    if res == 'exit':
        break
    process = c.Win32_Process()[int(res)-1]
    try:
        for i in psutil.Process(process.ProcessId).memory_maps():
            print(re.findall(r'[\w]*\..*',i.path)[0])
    except:
        print('You have not got acess)')
    finally:
        print('_______________________________________________')