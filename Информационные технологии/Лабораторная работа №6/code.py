import winreg as wreg

def ls (pkey): #вывод содержиого папки в реестре
    i = 0
    while True:
        try:
            key = wreg.EnumKey(pkey, i)
            print(key)
            i += 1
        except WindowsError:
            break

def edit(key, name): #функция для записи переменной
    wreg.SetValueEx(key, 'name', 0, wreg.REG_SZ, name)

def read (pkey): #функция для считывания переменной
    try:
        return wreg.QueryValueEx(pkey, 'name')[0]
    except:
        return 'NoName'

pkey = wreg.OpenKey(wreg.HKEY_CURRENT_USER, "Software\\lab_6",0,wreg.KEY_ALL_ACCESS)
name = read(pkey)
print('Your last name was:',name)
name = input('Enter your new name:\n')
edit(pkey,name)



