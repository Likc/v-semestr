#!/bin/bash

echolist()
{
	file="/home/likc/prog/DataBase.txt"
	count=1
	while read line
	do
		echo "$count" "$line"
		let "count = count + 1"
	done < $file

	menu;
}

sorting()
{
	echo -e "Chouse param. for sort:\n 1)Sourname 2)Name 3)Year of birth 4)City 5)Sex"
	read answer
	case "$answer" in
		"1") 
			sort -k1 DataBase.txt > tmp.txt
			mv tmp.txt DataBase.txt
			;;
		"2")
			sort -k2 DataBase.txt > tmp.txt
			mv tmp.txt DataBase.txt
			;;
		"3")
			sort -nk3 DataBase.txt > tmp.txt
			mv tmp.txt DataBase.txt
			;;
		"4")
			sort -k4 DataBase.txt > tmp.txt
			mv tmp.txt DataBase.txt
			;;
		"5")
			sort -k5 DataBase.txt > tmp.txt
			mv tmp.txt DataBase.txt
			;;
	esac
	echolist
}

edition()
{
	echo "Enten sourname:"
	read sourname;
	echo "Enten name:"
	read name;
	echo "Enten your year of birth:"
	read year;
	if ! [[ "$year" =~ [0-9]+ ]]
		then menu
		return 0;
	fi
	echo "Enten your place of birth:"
	read city;
	echo "Enten your sex (M|F):"
	read sex;
	if ! [[ "$sex" =~ M|F ]]
		then menu
		return 0
	fi
	echo -e $sourname"\t"$name"\t"$year"\t"$city"\t"$sex >> DataBase.txt
	menu;
}

delete()
{
	echo "What to delete:"
	read list
	IFS=' ' read -r -a array <<< "$list"
	res=""
	for element in "${array[@]}"
	do
		res="${res}""${element}d;" 
	done
	sed -i "${res::-1}" DataBase.txt
	echolist;
}

menu()
{
	echo -e "\n\e[1;34m1)\e[0mlist of students" "\e[1;32m2)\e[0mEdit students" "\e[1;33m3)\e[0mDelete" "\e[1;29m4)\e[0mSort by" "\e[1;35m5)\e[0mExit";
	read answer;
	case "$answer" in
		"1") 
			echolist
			;;
		"2")
			edition
			;;
		"3")
			delete
			;;
		"4")
			sorting
			;;
		"5")
			return 0
			;;
	esac
	menu;
}

menu





