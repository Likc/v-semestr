﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;

namespace programm
{
    public partial class Form1 : Form
    {
        const string userRoot = "HKEY_CURRENT_USER";
        const string subkey = "Software\\lab_7";
        const string keyName = userRoot + "\\" + subkey;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Text = textBox1.Text;
            Registry.SetValue(keyName, "Text", this.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string newname = (string) Registry.GetValue(keyName, "Text","NoSuch");
            this.Text = newname;
            int newwidth = (int) Registry.GetValue(keyName, "Width",10);
            this.Width = newwidth;
            int newheight = (int)Registry.GetValue(keyName, "Height", 10);
            this.Height = newheight;

            const string wallpaper = userRoot + "\\Control Panel\\Desktop";
            try
            {
                string background = (string)Registry.GetValue(wallpaper, "Wallpaper", "NoSuch");
                this.BackgroundImage = Image.FromFile(background);
            }
            catch
            {
                
            }
            try
            {
                string background = (string)Registry.GetValue(wallpaper, "WallPaper", "NoSuch");
                this.BackgroundImage = Image.FromFile(background);
            }
            catch 
            {

            }

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Registry.SetValue(keyName, "Width", this.Width);
            Registry.SetValue(keyName, "Height", this.Height);
        }
    }
}
